import React from 'react';
import EmployeeOverview from './EmployeeOverview';
import { shallow } from 'enzyme';
describe('renders EmployeeOverview', () => {
    it('Renders EmployeeOverview correctly', () => {
        const component = shallow(<EmployeeOverview employeeName="test" />);
        expect(component).toMatchSnapshot();
    });
});
