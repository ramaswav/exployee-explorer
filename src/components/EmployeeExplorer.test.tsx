import React from 'react';
import { shallow } from 'enzyme';
import EmployeeExplorer from './EmployeeExplorer';

describe('renders EmployeeExplorer', () => {
    it('Renders EmployeeExplorer correctly', () => {
        const component = shallow(<EmployeeExplorer />);
        expect(component).toMatchSnapshot();
    });
});
