import React, { useRef } from 'react';
import { useHistory, withRouter } from 'react-router-dom';

const EmployeeExplorer: React.FC = () => {
    const textInputNameRef = useRef<HTMLInputElement>(null);
    const history = useHistory();
    const searchEmployee = (event: React.FormEvent) => {
        event.preventDefault();
        const employeeName: string = textInputNameRef.current!.value;
        history.push('/overview/' + employeeName);
    };
    return (
        <>
            <div className="main-header">
                <h1 className="text-center uppercase">
                    Employee Explorer <i className="fa fa-users"></i>
                </h1>
            </div>
            <form className="search">
                <input type="text" placeholder="Search.." name="name" ref={textInputNameRef} />
                <button onClick={searchEmployee}>
                    <i className="fa fa-search"></i>
                </button>
            </form>
        </>
    );
};
export default withRouter(EmployeeExplorer);
