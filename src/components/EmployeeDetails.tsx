import React, { useEffect, useState } from 'react';
import * as _ from 'lodash';
import { useEmployee } from '../contexts/employee';
interface Props {
    employeeName: string;
}

const ADDITIVE_URL = 'http://api.additivasia.io/api/v1/assignment/employees/';
const OVERVIEW_URL = '/overview/';

const EmployeeDetails: React.FC<Props> = props => {
    const { directSubordinates, setEmployee } = useEmployee();
    const [subReady, setSubReady] = useState<Boolean>(false);
    const [employeeDesignation, setEmployeeDesignation] = useState<String>('');
    const empList = (subordinates: string[], list: string[]) => {
        subordinates.forEach((subordinate: string) => {
            list.push(subordinate);
            fetch(ADDITIVE_URL + subordinate)
                .then(response => response.json())
                .then(json => {
                    directSubordinates.push(subordinate);
                    setEmployee({ designation: json[0], directSubordinates });
                    if (json[0] === 'employee') {
                        return list;
                    } else {
                        return empList(json[1]['direct-subordinates'], list);
                    }
                })
                .catch((error: Error) => console.log(error));
        });
    };
    useEffect(() => {
        fetch(ADDITIVE_URL + props.employeeName)
            .then(response => response.json())
            .then(json => {
                if (json[0] && json[0] !== 'employee') {
                    // Build DirectSubordinates
                    empList(json[1]['direct-subordinates'], json[1]['direct-subordinates']);
                    setSubReady(true);
                }
                setEmployeeDesignation(json[0]);
                setEmployee({ designation: json[0], directSubordinates });
            })
            .catch(() => setSubReady(false));
    }, []);

    const renderDesignation = () => {
        return (
            <>
                <div className="second-header">
                    <h4>
                        Designation: <span>{employeeDesignation}</span>
                    </h4>
                </div>
            </>
        );
    };

    const renderSubordinates = () => {
        // @ts-ignore
        const renderList: string[] = _.uniq(directSubordinates);
        const renderSubordinatesHtml = renderList.map((sub: string, index) => {
            return (
                <tr key={index}>
                    <td>{sub}</td>
                    <td>
                        <a href={OVERVIEW_URL + sub}>{sub}</a>
                    </td>
                </tr>
            );
        });
        return (
            <>
                <div className="second-header">
                    <h4>
                        Subordinates of employee <b>{props.employeeName}</b>
                    </h4>
                </div>
                <table id="employees">
                    <tbody>
                        <tr>
                            <th>Employee Name</th>
                            <th>Employee Profile</th>
                        </tr>
                        {renderSubordinatesHtml}
                    </tbody>
                </table>
            </>
        );
    };
    return (
        <>
            {employeeDesignation && renderDesignation()}
            {subReady && renderSubordinates()}
            {!employeeDesignation && (
                <>
                    <div className="second-header">
                        <h4>Employee Not Found</h4>
                    </div>
                </>
            )}
        </>
    );
};
export default EmployeeDetails;
