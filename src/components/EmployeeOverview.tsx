import React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import EmployeeDetails from './EmployeeDetails';

interface SearchProps extends RouteComponentProps<any> {
    employeeName: String;
}

const EmployeeOverview: React.FC<SearchProps> = props => {
    return (
        <>
            <div className="main-header">
                <h1 className="text-left uppercase">
                    Employee Overview <i className="fa fa-address-card"></i>
                </h1>
            </div>

            <EmployeeDetails employeeName={props.match.params.employeeName} />
            <Link to="/">
                <i className="back-icon fa fa-arrow-circle-left"></i>
            </Link>
        </>
    );
};

export default withRouter(EmployeeOverview);
