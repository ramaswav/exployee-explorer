import React from 'react';
import App from './App';
import { shallow } from 'enzyme';

describe('renders App Correctly', () => {
    it('Renders EmployeeExplorer correctly', () => {
        const component = shallow(<App />);
        expect(component).toMatchSnapshot();
    });
});
