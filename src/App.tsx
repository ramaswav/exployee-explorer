import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import EmployeeExplorer from './components/EmployeeExplorer';
import EmployeeOverview from './components/EmployeeOverview';
import { EmployeeProvider } from './contexts/employee';

const App: React.FC = () => {
    return (
        <div className="main">
            <BrowserRouter>
                <Switch>
                    <Route exact path="/">
                        <EmployeeExplorer />
                    </Route>
                    <Route path="/overview/:employeeName">
                        <EmployeeProvider>
                            <EmployeeOverview employeeName="" />
                        </EmployeeProvider>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;
