import { Employee, Action } from './types';

export const employReducer: React.Reducer<Employee, Action> = (state, action) => {
    switch (action.type) {
        case 'SET_EMPLOYEE': {
            return {
                ...state,
                ...action.payload.employee,
            };
        }
        default: {
            return state;
        }
    }
};
