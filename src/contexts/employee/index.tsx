import React, { createContext, useContext, useReducer, useCallback } from 'react';
import { employReducer } from './reducer';
import { Context } from './types';

export const EmployeeContext = createContext<Context>({} as Context);

export const useEmployee = () => useContext(EmployeeContext);

export const EmployeeProvider: React.FC = ({ children }) => {
    const [{ designation, directSubordinates }, dispatch] = useReducer(employReducer, {
        designation: '',
        directSubordinates: [],
    });

    const setEmployee: Context['setEmployee'] = useCallback(
        employee => {
            dispatch({
                type: 'SET_EMPLOYEE',
                payload: { employee },
            });
        },
        [dispatch],
    );

    return (
        <EmployeeContext.Provider
            value={{
                designation,
                directSubordinates,
                setEmployee,
            }}
        >
            {children}
        </EmployeeContext.Provider>
    );
};
