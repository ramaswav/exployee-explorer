export interface Employee {
    designation: string;
    directSubordinates: string[];
}

interface IAction<T extends string, P extends object> {
    type: T;
    payload: P;
}

export type Action = IAction<'SET_EMPLOYEE', { employee: Employee }>;

export interface Context extends Employee {
    setEmployee: (employee: Employee) => void;
}
